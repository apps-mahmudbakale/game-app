const electron = require('electron');

const url = require('url');

const path = require('path');


const {app, BrowserWindow, Menu} = electron;


let   mainWindow;


//Listen for app to be ready

app.on('ready', function(){
	const {width, height} = electron.screen.getPrimaryDisplay().workAreaSize;
	mainWindow = new BrowserWindow({width, height});

	// Load html in to window
var menu = Menu.buildFromTemplate([
	{
		label:'Menu',
		submenu:[
			{label:'mahmud'},
			{label:'bakale'}
		]
	}

	])
	
	Menu.setApplicationMenu(menu);

	mainWindow.loadURL(url.format({
		pathname: path.join(__dirname,'src/mainwindow.html'),
		protocol: 'file:',
		slashes: true
	}));
});