const startButton = document.getElementById('start-btn')
const nextButton = document.getElementById('next-btn')
const questionContainerElement = document.getElementById('question-container')
const questionElement = document.getElementById('question')
const answerButtonsElement = document.getElementById('answer-buttons')
const correctaudio = new Audio('../assets/media/Minion Ahahaha Sound Effect.mp3');
const wrongaudio = new Audio('../assets/media/Minion Hohoho Sound Effect.mp3');
var score = 0;

let shuffledQuestions, currentQuestionIndex

startButton.addEventListener('click', startGame)
nextButton.addEventListener('click', () => {
  currentQuestionIndex++
  setNextQuestion()
  correctaudio.pause();
  wrongaudio.pause();
})

function startGame() {
  startButton.classList.add('hide')
  shuffledQuestions = questions.sort(() => Math.random() - .5)
  currentQuestionIndex = 0
  questionContainerElement.classList.remove('hide')
  setNextQuestion()
}

function setNextQuestion() {
  resetState()
  showQuestion(shuffledQuestions[currentQuestionIndex])
}

function showQuestion(question) {
  questionElement.innerText = question.question
  question.answers.forEach(answer => {
    const button = document.createElement('button')
    button.innerHTML = '<span class="btn-label"><i class="fa fa-chevron-right"></i> ' +answer.text+'</span>'
    button.classList.add('element-animation')
    button.classList.add('btn')
    button.classList.add('btn-lg')
    button.classList.add('btn-primary')
    button.classList.add('btn-block')
    if (answer.correct) {
      button.dataset.correct = answer.correct
    }
    button.addEventListener('click', selectAnswer)
    answerButtonsElement.appendChild(button)
  })
}

function resetState() {
  clearStatusClass(document.body)
  nextButton.classList.add('hide')
  while (answerButtonsElement.firstChild) {
    answerButtonsElement.removeChild(answerButtonsElement.firstChild)
  }
}

function selectAnswer(e) {
  const selectedButton = e.target
  const correct = selectedButton.dataset.correct
  if (correct == 'true') {
    score += 5;
    correctaudio.play();
    localStorage.scores = score;
    let userscore = document.getElementById('score');
    userscore.innerText = score;
    const storage = JSON.parse(localStorage.getItem('highscore'));
    var player = localStorage.players;
    var scores = parseInt(localStorage.scores);

    let obj = {
      highscores:[]
    };

    if (storage==null) {

          obj.highscores.push({
            player:player,
            score:scores
          });

          localStorage.setItem('highscore', JSON.stringify(obj));

        } else {
          highscore = JSON.parse(localStorage.getItem('highscore'));

                for (var i = 0; i < highscore.highscores.length; i++) {
                      if (highscore.highscores[i].player === player) {
                          let num = parseInt(highscore.highscores[i].score) 
                          let newscore = parseInt(num + scores);
                          highscore.highscores[i].score = newscore;
                          console.log(newscore);
                      }else{
                        highscore.highscores.push({
                          player:player,
                          score:scores
                        });
                      }
                      break;
                }

          localStorage.setItem('highscore', JSON.stringify(highscore));
          console.log(highscore.highscores);
        }
  }else {
    wrongaudio.play()
  }
  setStatusClass(document.body, correct)
  Array.from(answerButtonsElement.children).forEach(button => {
    setStatusClass(button, button.dataset.correct)
  })
  if (shuffledQuestions.length > currentQuestionIndex + 1) {
    nextButton.classList.remove('hide')
  } else {
    startButton.innerHTML = '<i class="fa fa-random"></i> Restart'
    startButton.classList.remove('hide')
  }
}

function setStatusClass(element, correct) {
  clearStatusClass(element)
  if (correct) {
    element.classList.remove('btn-primary')
    element.classList.add('btn-success')
    element.classList.add('btn-correct')

  } else {
    element.classList.add('btn-danger')
  }
}

function clearStatusClass(element) {
  element.classList.remove('correct')
  element.classList.remove('wrong')
}



const questions = [
  {
    question: 'If The Word PLEASANT is represented by the number 56781843, Then What Word is represented by the number 5867?',
    answers: [
      { text: 'PALE', correct: true },
      { text: 'PALL', correct: false },
      { text: 'PANE', correct: false },
      { text: 'PEAL', correct: false },
      { text: 'PEEP', correct: false }
    ]
  },
  {
    question: 'If the word CHARISMATIC represent the number 48352673924, the Word CHIASMATA is represented by?',
    answers: [
      { text: '125367393', correct: false },
      { text: '125367398', correct: false },
      { text: '125367898', correct: false },
      { text: '482367393', correct: true },
      { text: '428367898', correct: false }
    ]
  },
  {
    question: 'If the word SLOW WEST SALE represent the number 8214 4385 8923, the Word LAST is represented by?',
    answers: [
      { text: '3914', correct: false },
      { text: '2985', correct: true },
      { text: '3923', correct: false },
      { text: '3853', correct: false },
      { text: '3893', correct: false }
    ]
  },
  {
    question: 'If ABCDEFGHI is represented as 123456789 what BIDDIC represent?',
    answers: [
      { text: '294493', correct: true },
      { text: '284563', correct: false },
      { text: '375582', correct: false },
      { text: '394492', correct: false }
    ]
  },
  {
    question: 'If PAINT is represented as 74128 and EXCEL as 93596 what ACCEPT would be?',
    answers: [
      { text: '455978', correct: true },
      { text: '547978', correct: false },
      { text: '554978', correct: false },
      { text: '735961', correct: false }
    ]
  }
]

