var player;
var players = [];
var getplayers;

const playersList = document.getElementById('players');

document.getElementById('form').addEventListener('submit', (e)=>{
	e.preventDefault();
	CreatePlayer();
	ReadPlayer();

	document.getElementById('form').reset();
});


function CreatePlayer() {
	const storage = JSON.parse(localStorage.getItem('players'));

	player = document.getElementById('player').value;

	if (player == '') {
		alert('Please Enter a Name');
	}else {

		if (storage==null) {

			players.push(player);

			localStorage.setItem('players', JSON.stringify(players));

		} else {

			players = JSON.parse(localStorage.getItem('players'));

			players.push(player);

			localStorage.setItem('players', JSON.stringify(players));

			window.location ='game.html');
		}
	}
}

function ReadPlayer() {
	playersList.innerHTML='';

	getplayers = JSON.parse(localStorage.getItem('players'));

	if (getplayers!=null) {
			for (var i = 0; i < getplayers.length; i++) {
				playersList.innerHTML+=`
					<li class="list-group-item">${getplayers[i]}</li>
				`
			}
	}
}
