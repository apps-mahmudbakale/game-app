class User:
	def __init__(self, name, email,age):
		self.name = name
		self.email = email
		self.age = age

	def greetings(self):
		return f'My Name is {self.name} and I am {self.age}'

	def has_birthday(self):
		self.age +=1

class Customer(User):
	"""docstring for Customer"""
	def __init__(self, name, email,age):
		self.name = name
		self.email = email
		self.age = age
		self.balance = 0

	def set_balance(self, balance):
		self.balance = balance

	def greetings(self):
		return f'My Name is {self.name} and I am {self.age} and my Balance is {self.balance}'


bakale = User('Bakale Mahmud', 'bakale.mahmud@gmail.com', 27)
mahmud = Customer('Mahmud Bakale', 'mahmudbakale@gmail.com', 31)

bakale.has_birthday()

mahmud.set_balance(500000)

print(bakale.greetings())
print(mahmud.greetings())

myFile = open('myFile.txt','w')

print('Name:', myFile.name)
print('Is Closed:', myFile.closed)
print('Opening Mode:', myFile.mode)

myFile.write('I love Programming with Python')
myFile.write('and i love Javascript')
myFile.close()
myFile = open('myFile.txt','a')
myFile.write(' i also like  PHP')
myFile.close()


