const startButton = document.getElementById('start-btn')
const nextButton = document.getElementById('next-btn')
const questionContainerElement = document.getElementById('question-container')
const questionElement = document.getElementById('question')
const answerButtonsElement = document.getElementById('answer-buttons')
const correctaudio = new Audio('../assets/media/Minion Ahahaha Sound Effect.mp3');
const wrongaudio = new Audio('../assets/media/Minion Hohoho Sound Effect.mp3');
var score = 0;

let shuffledQuestions, currentQuestionIndex

startButton.addEventListener('click', startGame)
nextButton.addEventListener('click', () => {
  currentQuestionIndex++
  setNextQuestion()
  correctaudio.pause();
  wrongaudio.pause();
})

function startGame() {
  startButton.classList.add('hide')
  shuffledQuestions = questions.sort(() => Math.random() - .5)
  currentQuestionIndex = 0
  questionContainerElement.classList.remove('hide')
  setNextQuestion()
}

function setNextQuestion() {
  resetState()
  showQuestion(shuffledQuestions[currentQuestionIndex])
}

function showQuestion(question) {
  questionElement.innerHTML = question.question
  question.answers.forEach(answer => {
    const button = document.createElement('button')
    button.innerHTML = '<span class="btn-label"><i class="fa fa-chevron-right"></i> ' +answer.text+'</span>'
    button.classList.add('element-animation')
    button.classList.add('btn')
    button.classList.add('btn-lg')
    button.classList.add('btn-primary')
    button.classList.add('btn-block')
    if (answer.correct) {
      button.dataset.correct = answer.correct
    }
    button.addEventListener('click', selectAnswer)
    answerButtonsElement.appendChild(button)
  })
}

function resetState() {
  clearStatusClass(document.body)
  nextButton.classList.add('hide')
  while (answerButtonsElement.firstChild) {
    answerButtonsElement.removeChild(answerButtonsElement.firstChild)
  }
}

function selectAnswer(e) {
  const selectedButton = e.target
  const correct = selectedButton.dataset.correct
  if (correct == 'true') {
    score += 5;
    correctaudio.play();
    localStorage.scores = score;
    let userscore = document.getElementById('score');
    userscore.innerText = score;
    const storage = JSON.parse(localStorage.getItem('highscore'));
    var player = localStorage.players;
    var scores = parseInt(localStorage.scores);

    let obj = {
      highscores:[]
    };

    if (storage==null) {

          obj.highscores.push({
            player:player,
            score:scores
          });

          localStorage.setItem('highscore', JSON.stringify(obj));

        } else {
          highscore = JSON.parse(localStorage.getItem('highscore'));

                for (var i = 0; i < highscore.highscores.length; i++) {
                      if (highscore.highscores[i].player === player) {
                          let num = parseInt(highscore.highscores[i].score) 
                          let newscore = parseInt(num + scores);
                          highscore.highscores[i].score = newscore;
                          console.log(newscore);
                      }else{
                        highscore.highscores.push({
                          player:player,
                          score:scores
                        });
                      }
                      break;
                }

          localStorage.setItem('highscore', JSON.stringify(highscore));
          console.log(highscore.highscores);
        }
  }else {
    wrongaudio.play()
  }
  setStatusClass(document.body, correct)
  Array.from(answerButtonsElement.children).forEach(button => {
    setStatusClass(button, button.dataset.correct)
  })
  if (shuffledQuestions.length > currentQuestionIndex + 1) {
    nextButton.classList.remove('hide')
  } else {
    startButton.innerHTML = '<i class="fa fa-random"></i> Restart'
    startButton.classList.remove('hide')
  }
}

function setStatusClass(element, correct) {
  clearStatusClass(element)
  if (correct) {
    element.classList.remove('btn-primary')
    element.classList.add('btn-success')
    element.classList.add('btn-correct')

  } else {
    element.classList.add('btn-danger')
  }
}

function clearStatusClass(element) {
  element.classList.remove('correct')
  element.classList.remove('wrong')
}



const questions = [
  {
    question: '<img class="img img-thumbnail" width = "400px" src="../assets/images/subset1.jpg"><br> How many total people are represented in the Diagram?',
    answers: [
      { text: '100', correct: false },
      { text: '86', correct: true },
      { text: '98', correct: false },
    ]
  },
  {
    question: '<img class="img img-thumbnail" width = "400px" src="../assets/images/subset1.jpg"><br> How many people like Country?',
    answers: [
      { text: '10', correct: false },
      { text: '25', correct: false },
      { text: '33', correct: true },
    ]
  },
  {
    question: '<img class="img img-thumbnail" width = "400px" src="../assets/images/subset1.jpg"><br> If one person is chosen at random, what is the probability that that person will like rap P(rap) =',
    answers: [
      { text: '1/2', correct: false },
      { text: '43/86', correct: true },
      { text: '15/86', correct: false },
    ]
  },
  {
    question: '<img class="img img-thumbnail" src="../assets/images/subset1.jpg" width = "400px"> <br> If one person is choosen at random, what is the propability that that person will like country or rock music P(country or rock) =',
    answers: [
      { text: '50/86', correct: true },
      { text: '43/86', correct: false },
      { text: '32/86', correct: false },
    ]
  },
  {
    question: '<img class="img img-thumbnail" src="../assets/images/subset1.jpg" width = "400px"> <br> If one person is choosen ata random what is the propability that that person will like country and rock music P(country and rock) =',
    answers: [
      { text: '15/86', correct: true },
      { text: '54/86', correct: false },
      { text: '55/86', correct: false },
    ]
  }
]

