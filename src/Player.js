var player;

document.getElementById('form').addEventListener('submit', (e)=>{
	e.preventDefault();
	CreatePlayer();
	document.getElementById('form').reset();
});


function CreatePlayer() {
	player = document.getElementById('player').value;

	if (player == '') {
		alert('Please Enter a Name');
	}else {
		console.log(player);
		localStorage.players = player;
		window.location ='gameMenu.html';
	}
}

